#!/bin/sh

set -euo pipefail # fail on error , debug all lines
# e = stops on script error
# u = stops on undefined variable
# o pipefail = stops if any command in pipe failed

argCount=$#
if [ $argCount -eq 0 ]; then
    REGISTRY_SERVER="docker.io/jeffwayne256"
    IMAGE_NAME=$(basename `pwd`)
    IMAGE_TAG="latest"
    FULL_IMAGE_NAME=$REGISTRY_SERVER/$IMAGE_NAME:$IMAGE_TAG
else
    FULL_IMAGE_NAME=$1
fi

test() {
    docker run --rm $FULL_IMAGE_NAME "$@"
}

echo "\nTesting python numpy installation"
test python -c 'import numpy'

echo  "\nAll test passed!"
