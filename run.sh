#!/bin/bash

set -euo pipefail # fail on error , debug all lines
# e = stops on script error
# u = stops on undefined variable
# o pipefail = stops if any command in pipe failed

REGISTRY_SERVER="docker.io/jeffwayne256"
IMAGE_NAME=$(basename `pwd`)
IMAGE_TAG="latest"

# Bind mount CODE_DIR if it is set
CODE_DIR=${CODE_DIR:-}
BIND_MNT_CMD=""
if [ -n "$CODE_DIR" ]; then
    # Since an external directory has been specified, we need to bind mount it
    # and set the working directory to it using the -w /usr/src flag
    BIND_MNT_CMD="-v $CODE_DIR:/usr/src -w /usr/src"
fi

CMD="/usr/local/bin/docker run -it --rm ${BIND_MNT_CMD} ${REGISTRY_SERVER}/${IMAGE_NAME}:${IMAGE_TAG}"

eval $CMD
