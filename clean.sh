#!/bin/bash

set -euo pipefail # fail on error , debug all lines
# e = stops on script error
# u = stops on undefined variable
# o pipefail = stops if any command in pipe failed

REGISTRY_SERVER="docker.io"
IMAGE_NAME=jeffwayne256/$(basename `pwd`)
IMAGE_TAG="latest"

echo "Removing $REGISTRY_SERVER/$IMAGE_NAME:$IMAGE_TAG"
/usr/local/bin/docker rmi ${REGISTRY_SERVER}/${IMAGE_NAME}:"${IMAGE_TAG:-0.0.0}"
